import CongNhan from "./cong_nhan.js";
import ConNguoi from "./con_nguoi.js";
import HocSinh from "./hoc_sinh.js";
import SinhVien from "./sinh_vien.js";

// Khởi tạo đối tượng con người
var conNguoi = new ConNguoi("Hùng", "12/10/1993", "Thanh Hóa");
console.log(conNguoi.getThongTin());

// Khởi tạo đối tượng học sinh
var hocSinh = new HocSinh("Đại Học Bách Khoa", "CN206", "0968888666", "Hùng", "12/10/1993", "Thanh Hóa");
console.log(hocSinh.getThongTinHocSinh());
console.log(hocSinh instanceof ConNguoi);

// Khởi tạo đối tượng sinh viên
var sinhVien = new SinhVien("Tự Động Hóa", "20161188", "Đại Học Bách Khoa", "CN206", "0968888666", "Hùng", "12/10/1993", "Thanh Hóa");
console.log(sinhVien.getThongTinSinhVien());
console.log(sinhVien instanceof HocSinh);
console.log(sinhVien instanceof ConNguoi);

// Khởi tạo đối tượng công nhân
var congNhan = new CongNhan("Sửa xe máy", "Hà Nội", "5000000 VNĐ", "Hùng", "12/10/1993", "Thanh Hóa");
console.log(congNhan.getThongTinCongNhan());
console.log(congNhan instanceof ConNguoi);