import ConNguoi from "./con_nguoi.js";

// Tạo ra một class HocSinh kế thừa class ConNguoi
class HocSinh extends ConNguoi {
    // Hàm khởi tạo
    constructor(tenTruong, lop, soDienThoai, hoTen, ngaySinh, queQuan) {
        super(hoTen, ngaySinh, queQuan);
        this.tenTruong = tenTruong || "unknown";
        this.lop = lop || "unknown";
        this.soDienThoai = soDienThoai || "unknown"
    }
    // phương thức
    getThongTinHocSinh() {
        return `${this.getThongTin()}, trường: ${this.tenTruong}, lớp: ${this.lop}, số điện thoại: ${this.soDienThoai}`;
    }
}
export default HocSinh;